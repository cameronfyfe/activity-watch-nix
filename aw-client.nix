{ python3 }:
{ src, version, py-libs, aw-core }:

python3.pkgs.buildPythonPackage {
  pname = "aw-client";

  format = "pyproject";

  inherit src version;

  nativeBuildInputs = [
    python3.pkgs.poetry
  ];

  propagatedBuildInputs = [ aw-core ] ++ (with python3.pkgs; [
    click
    requests
    tabulate
  ]) ++ (with py-libs; [
    persist-queue
  ]);
}
