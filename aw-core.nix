{ python3 }:
{ src, version, py-libs }:

python3.pkgs.buildPythonPackage rec {
  pname = "aw-core";

  format = "pyproject";

  inherit src version;

  nativeBuildInputs = [
    python3.pkgs.poetry
  ];

  preBuild = "pip list";

  propagatedBuildInputs = with python3.pkgs; [
    appdirs
    deprecation
    jsonschema
    peewee
    rfc3339-validator
    strict-rfc3339
    tomlkit
  ] ++ (with py-libs; [
    iso8601
    python-json-logger
    TakeTheTime
    timeslot
  ]);
}
