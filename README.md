# activity-watch-nix

Nix flake for [Activity Watch](https://activitywatch.net) ([github](https://github.com/ActivityWatch/activitywatch)).

Currently this uses overlays from other flakes for access to nightly Rust, and for pulling in rust crates and npm packages. If the project moves over to stable Rust for the server it should be pretty reasonable to rework into something that could be included in nixpkgs.

# Usage

## Direct

Start activity-watch server + watchers:

    nix run --inputs-from gitlab:cameronfyfe/activity-watch-nix .#aw-start


Stop activity-watch server + watchers:

    nix run --inputs-from gitlab:cameronfyfe/activity-watch-nix .#aw-stop


## NixOS Config

`aw-start` and `aw-stop` can be added to PATH with the following nixos config.

### Add activity-watch to flake inputs:

`flake.nix`
```nix
{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    activity-watch.url = "gitlab:cameronfyfe/activity-watch-nix";
    ...
  };

  ...
}
```

### Add activity-watch overlay and package from the overlay.

`configuration.nix`
```nix
{ pkgs, activity-watch, ... }:

{
  ...

  nixpkgs.overlays = [ ... ] ++ activity-watch.overlays;

  environment.systemPackages = [
    ...
    pkgs.activity-watch.activity-watch
    ...
  ];

  ...
}

```
