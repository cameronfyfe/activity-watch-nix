{ python3 }:

{

  iso8601 = python3.pkgs.buildPythonPackage rec {
    pname = "iso8601";
    version = "1.0.2";

    src = python3.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "J/UDIg5oRdnblU+yErlbA2LYt+bBsjJqhwYcPek1lLE=";
    };

    doCheck = false;
  };

  persist-queue = python3.pkgs.buildPythonPackage rec {
    pname = "persist-queue";
    version = "0.6.0";

    src = python3.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "5z3WJUXTflGSR9ljaL+lxRD95mmZozjW0tRHkNwQ+Js=";
    };

    doCheck = false;
  };

  TakeTheTime = python3.pkgs.buildPythonPackage rec{
    pname = "TakeTheTime";
    version = "0.3.1";

    src = python3.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "2+MEU6G1lqOPni4/qOGtxa8tv2RsoIN61cIFmhb+L/k=";
    };

    doCheck = false;
  };

  timeslot = python3.pkgs.buildPythonPackage rec {
    pname = "timeslot";
    version = "0.1.2";

    src = python3.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "oqyZhlfj87nKkodXtJBq3SwFOQxfwU7XkruQKNCFR7E=";
    };

    doCheck = false;
  };

  tomlkit = python3.pkgs.buildPythonPackage rec {
    pname = "tomlkit";
    version = "0.6.0";

    src = python3.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "dPl2kIAw/xZMCqHtq+O/g+oASz2qWwlAuchqBgwATpo=";
    };

    doCheck = false;
  };

  python-json-logger = python3.pkgs.buildPythonPackage rec {
    pname = "python-json-logger";
    version = "0.1.11";

    src = python3.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "t6MRYvKgGWWl77lEU85pIw7SCEaLC7x/38VubY3y4oE=";
    };

    doCheck = false;
  };

  rfc3339-validator = python3.pkgs.buildPythonPackage rec {
    pname = "rfc3339-validator";
    version = "0.1.4";

    src = python3.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "t6MRYvKgGWWl77lEU85pIw7SCEaLC7x/38VubY3y4oE=";
    };

    doCheck = false;
  };

}
