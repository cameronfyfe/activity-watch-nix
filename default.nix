{ pkgs, stdenv, callPackage, fetchFromGitHub, import-cargo, killall, writeShellScript }:

let

  version = "0.12.0b2";

  src = fetchFromGitHub {
    owner = "ActivityWatch";
    repo = "activitywatch";
    rev = "v${version}";
    fetchSubmodules = true;
    sha256 = "sha256-qSdBMbJvUr8d3/uSGSZ13DKKxVAwnp/hP8GNoYvMHBI=";
  };

  py-libs = callPackage ./py-libs.nix { };

in
rec {

  aw-core = callPackage ./aw-core.nix { } {
    src = "${src}/aw-core";
    inherit version py-libs;
  };

  aw-client = callPackage ./aw-client.nix { } {
    src = "${src}/aw-client";
    inherit version py-libs aw-core;
  };

  aw-watcher-afk = callPackage ./aw-watcher-afk.nix { } {
    src = "${src}/aw-watcher-afk";
    inherit version py-libs aw-client;
  };

  aw-watcher-window = callPackage ./aw-watcher-window.nix { } {
    src = "${src}/aw-watcher-window";
    inherit version py-libs aw-client;
  };

  aw-server = callPackage ./aw-server.nix { } {
    src = "${src}/aw-server-rust";
    inherit version import-cargo;
  };

  aw-start = writeShellScript "aw-start" ''
    ${aw-server}/bin/aw-server &
    ${aw-watcher-afk}/bin/aw-watcher-afk &
    ${aw-watcher-window}/bin/aw-watcher-window &

    # stay alive
    while true; do sleep 60; done
  '';

  aw-stop = writeShellScript "aw-stop" ''
    for P in .aw-server-wrap .aw-watcher-afk .aw-watcher-win aw-start; do
      ${killall}/bin/killall $P
    done
  '';

  activity-watch = stdenv.mkDerivation {
    name = "activity-watch";
    phases = [ "installPhase" ];
    installPhase = ''
      mkdir -p $out/bin
      cp ${aw-start} $out/bin/aw-start
      cp ${aw-stop} $out/bin/aw-stop
    '';
  };

}
