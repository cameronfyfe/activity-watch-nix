{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    import-cargo.url = "github:edolstra/import-cargo";
    napalm = {
      url = "github:nix-community/napalm";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
  };

  outputs = inputs @ { self, ... }:

    let

      overlays = [
        inputs.rust-overlay.overlay
        (final: prev: {
          import-cargo = inputs.import-cargo;
        })
        inputs.napalm.overlay
        (final: prev: {
          activity-watch = prev.callPackage ./default.nix { };
        })
      ];

    in

    {
      inherit overlays;
    }
    //
    (inputs.flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:

      let

        pkgs = import inputs.nixpkgs {
          inherit system overlays;
        };

      in

      {
        packages = {
          default = pkgs.activity-watch.activity-watch;
          inherit (pkgs.activity-watch) aw-core aw-client aw-watcher-afk aw-watcher-window aw-server;
        };

        apps = {
          aw-start = {
            type = "app";
            program = "${pkgs.activity-watch.aw-start}";
          };
          aw-stop = {
            type = "app";
            program = "${pkgs.activity-watch.aw-stop}";
          };
        };

        devShell = pkgs.mkShell {
          nativeBuildInputs = with pkgs; [ ets nixpkgs-fmt ];
        };

      })
    );
}
