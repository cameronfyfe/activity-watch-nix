{ pkgs, stdenv, rust-bin, perl, pkg-config, openssl, python3, napalm, nodejs-16_x, makeWrapper }:
{ src, version, import-cargo }:

let

  rust = rust-bin.nightly.latest.default;
  crates = (import-cargo.builders.importCargo {
    lockFile = "${src}/Cargo.lock";
    inherit pkgs;
  }).cargoHome;

  aw-webui =
    napalm.buildPackage "${src}/aw-webui" rec {
      nodejs = nodejs-16_x;
      nativeBuildInputs = [
        python3
      ];

      npmCommands = [
        "npm install"
        "npm run build"
      ];

      # Help npm run smoother in build sandbox
      preBuild = ''
        npm config set update-notifier false
        npm config set audit false
      '';

      installPhase = ''
        mv dist $out
      '';
    };

in

stdenv.mkDerivation rec {
  name = "aw-server";
  inherit src;
  nativeBuildInputs = [ rust crates perl pkg-config makeWrapper ];
  buildInputs = [ openssl ];
  phases = [ "unpackPhase" "buildPhase" "installPhase" ];
  buildPhase = ''
    cargo build --release
  '';
  installPhase = ''
    mkdir -p $out/bin
    mkdir -p $out/share/${name}
    ln -s ${aw-webui} $out/share/${name}/static
    cp target/release/${name} $out/bin/
    wrapProgram $out/bin/${name} --prefix XDG_DATA_DIRS : "$out/share"
  '';
}

