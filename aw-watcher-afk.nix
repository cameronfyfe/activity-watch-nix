{ python3 }:
{ src, version, py-libs, aw-client }:

python3.pkgs.buildPythonApplication {
  name = "aw-watcher-afk";

  format = "pyproject";

  inherit src version;

  nativeBuildInputs = [
    python3.pkgs.poetry
  ];

  propagatedBuildInputs = [ aw-client ] ++ (with python3.pkgs; [
    xlib
    pynput
  ]);
}
